import argparse
import logging
import re
import time
import boto3
import paramiko
from retrying import retry
import slackweb
from scp import SCPClient
import os

# initializing types available sizes of images
instance_types = ['t2.nano', 't2.micro', 't2.small', 't2.medium', 't2.large', 'm4.large', 'm4.xlarge', 'm4.2xlarge',
                  'm4.4xlarge', 'm4.10xlarge', 'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge', 'c4.large',
                  'c4.xlarge', 'c4.2xlarge', 'c4.4xlarge', 'c4.8xlarge', 'c3.large', 'c3.xlarge', 'c3.2xlarge',
                  'c3.4xlarge', 'c3.8xlarge', 'g2.2xlarge', 'g2.8xlarge', 'r3.large', 'r3.xlarge', 'r3.2xlarge',
                  'r3.4xlarge', 'r3.8xlarge', 'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge', 'd2.xlarge',
                  'd2.2xlarge', 'd2.4xlarge', 'd2.8xlarge']


# initializing types of available ram sizes
memory_sizes = [ 0.5, 1, 2, 4, 8,
                 8, 16, 32, 64, 160,
                 3.75, 7.5, 15, 30,
                 3.75, 7.5, 15, 30, 60,
                 3.75, 7.5, 15, 30, 60,
                 15, 60,
                 15.25, 30.5, 61, 122, 244,
                 30.5, 61, 122, 244,
                 30.5, 61, 122, 244
                 ]

# initializing region name for boto3
boto3.setup_default_session(region_name='eu-west-1')
# providing provision.log file name which will be storing the log information
logging.basicConfig(filename='provision.log', level=logging.DEBUG, format='%(asctime)s - %(message)s')

# initializing variables e.g. ec2 and ec2/s3 clients which will later be used to access ec2 and s3 storage
logfile = ""
st = 0
new_instance=None
ec2 = boto3.resource('ec2')
ec2_client = boto3.client('ec2')
s3_client = boto3.client('s3')
terminate_instance_id = []
# initializing slack which will be used to send output to later be visible to running instance
slack = slackweb.Slack(url='https://hooks.slack.com/services/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')

# write message to the slack and logging with particular pattern
def write_message(message):
    print message
    logging.info(message)
    slack.notify(text="SOLR Product Categorizer: {}".format(message), channel='#background-jobs', username='cahootsy-bot',
                 icon_emoji=':snowboarder:')

# Filter ec2obj with the image id that we are interested in provided to function in parameter and returned to caller
def get_from_image_id(ec2obj, image_id):
    result = []
    for o in ec2obj.filter(Filters=[{'Name': 'image-id', 'Values': [image_id]}]):
		result.append(o)

    return result

# Get ec2obj whose description is matched and returned to caller
def get_with_description_matching(ec2obj, description):
    result = []
    for o in ec2obj.all():
        if re.match(description, o.description):
            result.append(o)

    return result	

# Filter ec2obj with Tag that we are interested in is provided to function in parameter and returned to caller
def get_from_tag(ec2obj, tag):
    for o in ec2obj.filter(Filters=[{'Name': 'tag:Name', 'Values': [tag]}]):
        return o

    return None	

# Filter ec2obj with the name that we are interested in provided to function in parameter and returned to caller
def get_from_name(ec2obj, name):
    for o in ec2obj.filter(Filters=[{'Name': 'name', 'Values': [name]}]):
        return o

    return None	

# create spot instance with provided image id and image type and spot price
# parameters essential for launched are initialed with required information
# information is logged along the execution with write_message function call
# EC2 waiters are used to wait for the instance until created
def create_spot_instance(image_id, image_type, spot_price):
    write_message("Creating {} spot instance using image ID {} at ${}".format(image_type, image_id, spot_price))
    spot_instance_request_id = ec2_client.request_spot_instances(
        SpotPrice=str(spot_price),
        LaunchSpecification={
            'ImageId': image_id,
            'KeyName': 'utility-remote-connect',
            'InstanceType': image_type,
            'Placement': {
                'AvailabilityZone': 'eu-west-1a',
            },
            'SubnetId': 'subnet-921cc5cb',
            'EbsOptimized': True,
            'Monitoring': {
                'Enabled': False
            },
            'IamInstanceProfile': {
                'Name': 'CTO'
            }

        }
    )['SpotInstanceRequests'][0]['SpotInstanceRequestId']

    write_message("Waiting for spot instance {} to be created".format(spot_instance_request_id))
    waiter = ec2_client.get_waiter('spot_instance_request_fulfilled')
    waiter.wait(SpotInstanceRequestIds=[spot_instance_request_id])
    write_message("Spot instance {} created".format(spot_instance_request_id))

    write_message("Fetching ID for spot instance request {}".format(spot_instance_request_id))
    instance_id = ec2_client.describe_spot_instance_requests(
        SpotInstanceRequestIds=[spot_instance_request_id]
    )['SpotInstanceRequests'][0]['InstanceId']
    write_message("Created instance ID {}".format(instance_id))

    return instance_id


# this function is there to create image with provided instance id and name and description
# messages are logged along with write_message call
# waiter are used until the image is created and execution will remain suspended
# image_id created is return to the caller
def create_instance_image(instance_id, name, description):
    write_message("Creating image for {} with name {}".format(instance_id, name))
    image_id = ec2_client.create_image(
        InstanceId=instance_id,
        Name=name,
        Description=description
    )['ImageId']

    write_message("Waiting for image {} to be created".format(image_id))
    waiter = ec2_client.get_waiter('image_available')
    waiter.wait(ImageIds=[image_id])
    write_message("Image {} created".format(image_id))

    return image_id

# Creates Snapshot with volume id and description
# messages are logged along with write_message call
# waiter are used until the instance is created and execution will remain suspended
# Snapshot_id created is return to the caller
def create_ebs_snapshot(volume_id, description):
    write_message("Creating spot instance of volume {} with description \"{}\"".format(volume_id, description))
    snapshot_id = ec2_client.create_snapshot(
        VolumeId=volume_id,
        Description=description
    )['SnapshotId']

    write_message("Waiting for snapshot {} to complete".format(snapshot_id))
    waiter = ec2_client.get_waiter('snapshot_completed')
    waiter.wait(SnapshotIds=[snapshot_id])
    write_message("Snapshot {} completed".format(snapshot_id))

    return snapshot_id

# Function call to get spot prices for particular instances type and other information like zone/region and
# filters information of particular type
def get_spot_price_for(instance_type):
    return ec2_client.describe_spot_price_history(
        InstanceTypes=[instance_type],
        AvailabilityZone='eu-west-1a',
        MaxResults=100,
        Filters=[{'Name': 'product-description', 'Values': ['Linux/UNIX (Amazon VPC)']}]
    )['SpotPriceHistory'][0]['SpotPrice']

# Deletes instance with particular instance information and terminates them
# messages are logged with writer_message function call
# removes old volumes acquired previously by instances
def delete_instance(instance):
    volume_ids = map(lambda x: x.id, list(instance.volumes.all()))

    write_message("Terminating old instance {}".format(instance.id))
    ec2_client.terminate_instances(InstanceIds=[instance.id])

    waiter = ec2_client.get_waiter('instance_terminated')
    waiter.wait(InstanceIds=[instance.id])

    for v in ec2.volumes.all():
        if v.id in volume_ids:
            write_message("Deleting old volume {}".format(v.id))
            ec2_client.delete_volume(VolumeId=v.id)

# This is to delete any previously old images created using the id of image passed as parameter
def delete_old_image(image):
    write_message("Deleting old image {}".format(image.id))
    ec2_client.deregister_image(
        ImageId=image.id
    )
    for s in get_with_description_matching(ec2.snapshots, ".* for " + image.id + " from .*"):
        write_message("Deleting snapshot {} for image {}".format(s.id, image.id))
        ec2_client.delete_snapshot(SnapshotId=s.id)

# This deletes old resources acquired by ec2 image
def delete_old_resources():
    image = get_from_name(ec2.images, 'SOLR Search Generator of SOLR Production')

    if image is not None:
        # Delete any instances based on this Image
        for i in get_from_image_id(ec2.instances, image.id):
            delete_instance(i)

# This function actually hold the over all execution and call respective functions to get the job of provision done and
# this function calls above functions to get the image and spot instance created
# this also bids for the instance with additional 0.005 to the price and displays current bid price with new bid price
# waiters are used to wait for instance to get started
# write_messages function calls keep logging the messages
# At the end instance id is returned for particular instance created
def provision(source_instance_name, image_type):
    delete_old_resources()

    image = get_from_name(ec2.images, 'category-rework-instance')
    if image is None:
        solr_production_instance = get_from_tag(ec2.instances, source_instance_name)
		
        current_time = time.strftime('%Y-%m-%d %H:%M:%S')

        image_id = create_instance_image(solr_production_instance.id, 'category-rework-instance',
                                         'Image of {} on {}'.format(source_instance_name, current_time))
    else:
        image_id = image.id

    min_spot_price = get_spot_price_for(image_type)
    spot_price = float(min_spot_price) + 0.005
    write_message("Current price ${}, bidding at ${}".format(min_spot_price, spot_price))
    instance_id = create_spot_instance(image_id, image_type, spot_price)

    write_message("Waiting for instance {} to start up".format(instance_id))
    waiter = ec2_client.get_waiter('instance_running')
    waiter.wait(InstanceIds=[instance_id])
    write_message("Instance {} started".format(instance_id))
    terminate_instance_id.append(instance_id)
    return ec2.Instance(instance_id)

# this method has 3 attempt retries with 10 second for each attempt to connect to the running instance
# before connecting the ssh env is setup appropriate keys and policies
@retry(stop_max_attempt_number=3, wait_fixed=10000)
def connect_to_host_shell(host_name, user_name):
    write_message("Connecting to {} as {}".format(host_name, user_name))
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys()
    ssh.connect(host_name, username=user_name)
    return ssh

# This method runs the shell command provided and passed to it as parameters
def run_shell(ssh_client, command, flg):
    write_message("Running: {}".format(command))
    stdin, stdout, stderr = ssh_client.exec_command(command)
    for l in stdout.readlines():
        logging.debug(l.rstrip())
        if flg == True :
            write_message(l)


# This function is holding key commands that will be executed once the connect to new instance has been completed
# scripts are called from here and "&" at the end of the command means the execution is branched
# for the remaining command the execution will remain suspended with wait for each command

def run_essentials_Scripts(ssh_client):
    run_shell(ssh_client, 'sudo apt-get update -y', False)
    run_shell(ssh_client, 'curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"', False)
    run_shell(ssh_client, 'sudo python get-pip.py', False)
    run_shell(ssh_client, 'sudo pip install re ; sudo pip install scp ; sudo pip install csv ; sudo pip install glob ;  sudo pip install boto ; sudo pip install boto3 ; sudo pip install remap ; sudo pip install numpy ; sudo pip install codecs ; sudo pip install urllib2 ; sudo pip install logging ; sudo pip install slackweb ; sudo pip install argparse ; sudo pip install simple_loader ; sudo pip install keras.models ; sudo pip install keras.optimizers ; sudo pip install keras.layers.core ; sudo pip install keras.layers.recurrent ; sudo pip install keras.layers.convolutional ;', False)

def run_SOLR_runProductCateogrization_script(ssh_client , newServer_IP):
    try:
        os.system(('scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -rp  * ubuntu@'+newServer_IP+':/home/ubuntu'))
    except OSError:
        write_message(("File not found : "+OSError))
    finally:
        write_message( ("Transfer of files to server "+newServer_IP+" is complete" ) )
    run_shell(ssh_client, 'chmod +x *.py', False)
    run_shell(ssh_client, 'sudo python map_similarity.py',False)

# Call terminate instance script
def run_TerminateInstane_script(ssh_client):
    run_shell(ssh_client, 'sudo python3 runTerminateInstance.py',False)

# This function call sets the memory of the ec2 image with appropriate type selected
def set_memory_for_solr(ssh_client, image):
    image_index = instance_types.index(image)
    image_memory_size = int(memory_sizes[image_index] * 1024 * 0.75)

# Get the files to the new server to run there
def copy_files_FROM_host(ssh_client, newServer_IP):
    write_message("Getting files from remote host")
    try:
        os.system(('scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@'+newServer_IP+':/home/ubuntu/result.csv .'))
    except OSError:
        write_message(("File not found : "+OSError))
    finally:
        write_message("Transfer complete")

# This function call parse the argument passed to provision.py (e.g. in this it will be the image type)
def parse_arguments():
    parser = argparse.ArgumentParser(description='Provision a SOLR instance for SOLR Search Generator')

    parser.add_argument('-i', '--image', help='the type of image to build (e.g. c4.large)', default='c4.large',
                        choices=instance_types)

    return parser.parse_args()

def tostr(str, other):
    return str + other

# This is similar to MAIN of the program where argument parse is called
# new_instance is returned from provision function call with provided image
# ssh_client is returned from the host with a function call to connect to host shell, IP address and user is provided
# Memory is set for provided host and image type provided
# Files are downloaded from S3 storage location that are required for the execution of map_similarity.py file
# scripts that we need to executed are defined in run_SOLR_SearchGen_script with shell control user passed as an argument
# At the end we are uploading the log file to S3 storage with provided location

# try:
    args = parse_arguments()
    new_instance = provision('solr-production.cahootsy.com', args.image)
    ssh_client = connect_to_host_shell(new_instance.public_ip_address, 'ubuntu')
    set_memory_for_solr(ssh_client, args.image)

    run_essentials_Scripts(ssh_client)

    s3_client.download_file('cahootsy-categorisation-input', 'adwords/data/datafeed_206139.csv', 'data/datafeed_206139.csv')
    s3_client.download_file('cahootsy-categorisation-input', 'adwords/data/df1.csv', 'data/df1.csv')
    s3_client.download_file('cahootsy-categorisation-input', 'adwords/data/df2.csv', 'data/df2.csv')
    s3_client.download_file('cahootsy-categorisation-input', 'adwords/data/df3.csv', 'data/df3.csv')
    s3_client.download_file('cahootsy-categorisation-input', 'adwords/data/df4.csv', 'data/df4.csv')

    run_SOLR_runProductCateogrization_script(ssh_client, new_instance.public_ip_address)
    copy_files_FROM_host(ssh_client,new_instance.public_ip_address)

# TODO: tomkad - remove this and see where the error is coming from
# except OSError:
#     pass
# except IOError:
#     pass
# except TypeError:
#     pass

# TODO: tomkad - remove this and see where the real error is coming from
# finally:
#     s3_client.upload_file('result.csv', 'cahootsy-categorisation-output', 'result.csv')
#     write_message("Successfully uploaded files  result.csv to S3 bucket")
#     write_message(  tostr("Termination Instance initiated : ", terminate_instance_id[0] ) )
#     os.system( tostr("python runTerminateInstance.py --instanceid ",terminate_instance_id[0]) )
#     s3_client.upload_file('runTerminateInstance.log', 'cahootsy-categorisation-output', 'runTerminateInstance.log')
