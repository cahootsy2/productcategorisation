import word_vector
import numpy as np
from simple_loader import *
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation,Reshape,Flatten,Masking
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD
from keras.optimizers import Adagrad
from keras.layers.convolutional import Convolution1D,MaxPooling1D
from remap import *


print("loading word vectors...")
word_vector.load_projections("vectors.txt","words.txt")
print("loading train data")
read_docs()
test = get_samples_removes(4000)


def compute_random_baseline():
    correct = 0.0
    for x in test:
        q = randint(0,len(categories)-1)
        ct = cat_list[q]
        if ct == x[1]:
            correct = correct + 1.0
    print "random baseline = " + str(correct / 4000.0)

def prediction2cat(vect):
    nth = np.argmax(vect)
    catid = cat_list[nth]
    catname = categories[catid]
    return(catid,catname)

def prediction2cats(vect):
    nths = vect.argsort()[-3:][::-1]
    lst = []
    for x in nths:
        catid = cat_list[x]
        catname = categories[catid]
        lst.append((catid,catname,vect[x]))
    return lst

def predict(model, sample):
    sample = sample2vector_unfolded_descr([sample])
    res = model.predict(sample[0],1)
    return res[0]

def save_test_samples():
    samples = sample2vector_unfolded_descr(test)
    res = model.predict(samples[0],200)
    f = open("samples.txt","w")
    i = 0
    for r in res:
        
        cats =  prediction2cats(r)
        st = ""
        for cat in cats:
            st = st + str(cat[0] )+ " [" + cat[1] + "] p=" + str(cat[2]) + " | "
        st = str(test[i][0]) + str(" ; ") + str(st) + "\n"
        f.write(st)
        i = i + 1
    f.close()
     
    
compute_random_baseline()
#triples = load_raw_triples("test_triples.txt")
print("creating model...")
model = Sequential()
model.add(Reshape((100,50),input_shape=(5000,)))
#model.add(Masking())
model.add(Convolution1D(nb_filter=200,filter_length=2,activation="relu",subsample_length=1))
model.add(MaxPooling1D(pool_length=2,border_mode='valid'))
model.add(Convolution1D(nb_filter=200,filter_length=2,activation="relu",subsample_length=1))
model.add(MaxPooling1D(pool_length=2,border_mode='valid'))
model.add(Convolution1D(nb_filter=200,filter_length=2,activation="relu",subsample_length=1))
model.add(MaxPooling1D(pool_length=2,border_mode='valid'))

model.add(Flatten())
model.add(Dense(700,init='uniform'))
model.add(Activation('tanh'))
model.add(Dense(len(categories),init='uniform'))
model.add(Activation('sigmoid'))
sgd = SGD(lr=0.01, decay=0, momentum=0.9, nesterov=False)
ada = Adagrad(lr=0.01, epsilon=1e-06)
model.compile(loss='mean_squared_error', optimizer=ada)
model.load_weights('class_model_LSTM_desc.h5')

print("training model...")
#mb = get_minibatch_from_sample_list(samples)
#res = model.predict(mb[0], batch_size=5)
#print (res)
#model.fit(mb[0], mb[1], nb_epoch=1000, batch_size=5, verbose=1)
ac = 0
ac2 = 0
prev_acc = 0
for k in range(0,500):
 for i in range(0,100):
     samples =   get_samples(300)

     mb = sample2vector_unfolded_descr (samples)
 
     objective_score = model.evaluate(mb[0], mb[1], batch_size=300,show_accuracy=True,verbose=0)
     ac = ac + objective_score[0]
     ac2 = ac2 + objective_score[1]
     model.train_on_batch(mb[0], mb[1],accuracy=True)
     #print (objective_score)
 print (str(k) + " " + "after step total" + str(ac) + " accuracy =" + str(ac2/100))
 ac2 = 0
 ac = 0
 samples = test
 mb_test =sample2vector_unfolded_descr (samples)
 objective_score = model.evaluate(mb_test[0], mb_test[1], batch_size=100,show_accuracy=True,verbose=0)
 sn = "no"
 if objective_score[1] > prev_acc:
     sn = "yes"
     prev_acc = objective_score[1]
     model.save_weights('class_model_LSTM_desc.h5',overwrite=True)
 print("validation accuracy = " +  str(objective_score[1]) + " " + sn)

 
 
