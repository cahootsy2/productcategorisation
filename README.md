# Software for mapping merhcant categories to CAHOOTSY categories

## Installation
The process was tested under Ubuntu Linux 14.04. It might work with other Linux distributions as well, that have appropriate Python version.

Clone repository in any folder on your Linux machine
### Dependencies
 Python 2.7.9

##Usage

### Crude category mapper 

 * Put all merchant csv files into data folder. Make sure, that they are in the correct format. See example files in data folder for reference. Remove example files before actual use.

 * Put reference category tree into cahootsy_tree.csv file. Make sure, that the tree file is in correct format.

 * cd to src directory of this code (same folder, as where README is located)

 * Run code by typing in the command-line:

python map_similarity.py

 * Retrieve results from result.csv file

## Notes
Trainable category mapper currently is not in valid state, because it was trained with old category tree and never updated since, due to the lack of training data. Hence, no instructions are provided regarding running trainable category mapper. 
