import csv
from random import randint
import word_vector
import numpy as np
import os
import sys


#with open('product_text.csv', 'rb') as f:
#    reader = csv.reader(f)
#    for row in reader:
 #       print row
docs = {}

def refine(string):
    string = string.lower()
    string = string.replace(".", " . ")
    string = string.replace(",", " , ")
    string = string.replace("!", " ! ")
    string = string.replace("?", " ? ")
    return string

def getScriptPath():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

test = "[48]58[Girls Shoes & Accessories]|[48]69[Boys Shoes & Accessories]'"
def get_cats(string):
    q = string.split('|')
    return q
def get_cat_id(string):
    q = string.split(']')
    if len(q)<2:
        print (q)
        return ("-1")
    d = q[1].split('[')
    if len(d) > 0:
     q1 = (q[1].split('['))[0]
    else:
     q1 = '-1'
    return q1

def get_cat_name(string):
    q = string.split(']')
    if len(q)>1:
     qw = q[1].split('[')
     if len(qw)>1:
      q1 = (q[1].split('['))[1]
      return q1
     return "none"
    else:
        return "none"

all_data = {}

single_products={}

def load_all_merchant_data(filename):
    with open(filename, 'rU') as f:
      i =0
      qx  = 0
     
      reader = csv.reader(f,delimiter=',')
      for row in reader:
         i = i + 1
         if i>1 and len(row)>4:
          qx = qx + 1
          if qx > 50000:
              print i
              qx = 0
         
          #print row[4]
          if len(row)>27:
           merchant_cat = row[6]
           if len(merchant_cat)>2:
            merchant_category_id = row[5]
            product_name = row[9]
            
            description = row[8]
            merchant_name = row[20]
            id_string =  merchant_cat + " | " + merchant_name + " | " + merchant_category_id 
            if id_string in all_data:
                all_data[id_string].append(product_name + " | " + description)
            else:
               all_data[id_string] = [product_name + " | " + description]
           else:
	     product_name = row[9]
	     description = row[8]
	     single_products[row[0]] = product_name + " | " + description
          
        



categories = {}
cat_indexes={}
cat_list = []
products =[]

def save_category_list():
    f = open(getScriptPath() + "/category_list.txt","w")
    for x in cat_list:
        f.write(x + " | " + categories[x] + "\n")
    f.close()

def load_category_list():
    f = open(getScriptPath() + "/category_list.txt","r")
    p  = f.readlines()
    i = 0
    for x in p:
        w = x.split(" | ")
        categories[w[0].strip()] = w[1].strip()
        cat_list.append(w[0].strip())
        cat_indexes[w[0].strip()] = i
        i = i + 1
    f.close()

    
        
        
      
    


def read_docs():
    with open('product_text.csv', 'rU') as f:
     i =0
     qx  = 0
     
     reader = csv.reader(f)
     for row in reader:
         i = i + 1
         if i>1 and len(row)>4:
          qx = qx + 1
          if qx > 50000:
              print i
              qx = 0
          productname = row[1]
          productcats = row[4]
          product_descr = row[2]
         # print productcats + " " + productname
          cat_id = get_cat_id(get_cats( productcats)[0])
          cat_name = get_cat_name(get_cats( productcats)[0]).strip()
          product = (productname.lower().strip(),cat_id,refine(product_descr))
          categories[cat_id] = cat_name
          
          products.append(product)
     print "loaded " + str(len(products)) + " products "
     i = 0
     for x in categories:
         cat_indexes[x] = i
         cat_list.append(x)
         i = i + 1

def get_samples(num_samples):
    samples = []
    for i in range (0, num_samples):
     q = randint(0,len(products)-1)
     samples.append(products[q])
    return samples

def get_samples_removes(num_samples):
    samples = []
    for i in range (0, num_samples):
     q = randint(0,len(products)-1)
     samples.append(products[q])
     del products[q]
    return samples

def sample2vector_simple(samples):
  X = np.zeros((len(samples),len(word_vector.project_text_mod(samples[0][0]))))
  Y = np.zeros((len(samples),len(categories)))
  i = 0
  for x in samples:
    sample = x[0]
    outcome = x[1]
    sample_data = word_vector.project_text_mod(sample)
    for j in range(0,len(sample_data)):
      X[i,j] = sample_data[j]

    index =  cat_indexes[outcome]          
    Y[i,index] = 1.0
    i=i+1
  return(X,Y)




def sample2vector_unfolded(samples):
  X = np.zeros((len(samples),1000))
  Y = np.zeros((len(samples),len(categories)))
  i = 0
  for x in samples:
    sample = x[0]
    outcome = x[1]
    sample_data = word_vector.project_text_stack(sample)
    for j in range(0,1000):
      if len(sample_data)-1>j:
       X[i,j] = sample_data[j]

    index =  cat_indexes[outcome]          
    Y[i,index] = 1.0
    i=i+1
  return(X,Y)


def sample2vector_unfolded_descr(samples):
  X = np.zeros((len(samples),5000))
  Y = np.zeros((len(samples),len(categories)))
  i = 0
  for x in samples:
    sample = x[0] + " . " + x[2]
    outcome = x[1]
    sample_data = word_vector.project_text_stack(sample)
    for j in range(0,5000):
      if len(sample_data)-1>j:
       X[i,j] = sample_data[j]

    index =  cat_indexes[outcome]          
    Y[i,index] = 1.0
    i=i+1
  return(X,Y)


def sample2vector_unfolded_descr_p(samples):
  X = np.zeros((len(samples),5000))
  Y = np.zeros((len(samples),len(categories)))
  i = 0
  for x in samples:
    sample = x[0] 
    outcome = x[1]
    sample_data = word_vector.project_text_stack(sample)
    for j in range(0,5000):
      if len(sample_data)-1>j:
       X[i,j] = sample_data[j]

   
    i=i+1
  return(X,Y)

def sample2vector_unfolded_p(samples):
  X = np.zeros((len(samples),1000))
  Y = np.zeros((len(samples),len(categories)))
  i = 0
  for x in samples:
    sample = x[0]
    outcome = x[1]
    sample_data = word_vector.project_text_stack(sample)
    for j in range(0,1000):
      if len(sample_data)-1>j:
       X[i,j] = sample_data[j]

   
    i=i+1
  return(X,Y)
       

