import codecs
from random import randint
from random import seed
import word_vector
import numpy as np
import urllib2

seed(65536)

def refines(string):
    string = string.replace(u".", u" . ")
    string = string.replace(u",", u" , ")
    string = string.replace(u"!", u" ! ")
    string = string.replace(u"?", u" ? ")
    string = string.replace(u"- ", u" ")
    
    return string

def wiki_loader(filename):
  f = codecs.open(filename,"r",'utf-8')
  p = []
  for line in f:
    q = randint(0,5)
    if q == 4:
     p.append(line.split(" "))
     #count = count + 1
  return p

def get_samples_bilang_model(wdata,numsamples):
    samples=[]
    for i in range(0,numsamples):

     q = randint(0,len(wdata)-1)
     
     line = wdata[q]
     if len(line)>6:
      start = randint(0,len(line)-6)
     
      rs = ((line[start],line[start+1],line[start+3],line[start+4]),line[start+2])
      samples.append(rs)
    return samples

def bilang_samples_to_mb(samples,dic):
    X = np.zeros((len(samples),200))
    Y= np.zeros((len(samples),len(dic)+1))
    i = 0
    for x in samples:
        sample = []
        w1 = (word_vector.project_word(x[0][0]))
        w2 = (word_vector.project_word(x[0][1]))
        w3 = (word_vector.project_word(x[0][2]))
        w4 = (word_vector.project_word(x[0][3]))
        for j in range(0,50):
            X[i,j] = w1[j]
        for j in range(50,100):
            X[i,j] = w2[j-50]
        for j in range(100,150):
            X[i,j] = w3[j-100]
        for j in range(150,200):
            X[i,j] = w4[j-150]
            
            
        num_word = word2bin_index(x[1],dic)
        Y[i,num_word] = 1.0
        i = i + 1
    return (X,Y)
                      
                      

def load_classification_dataset(positive_file,negative_file):
    f = codecs.open(positive_file,"r",'utf-8')
    p = f.readlines()
    for i in range(0,len(p)-1):
        p[i]  = (p[i].strip()).lower()

    f = codecs.open(negative_file,"r",'utf-8')
    p1 = f.readlines()
    for i in range(0,len(p1)-1):
        p1[i]  = (p1[i].strip()).lower()
    return(p,p1)

def load_singles(filename):
    f = codecs.open(filename,"r",'utf-8')
    p = f.readlines()
    for i in range(0,len(p)-1):
        p[i]  = refines ((p[i].strip()).lower())
    return p
    

def get_random_classif_samples(data,numsamples):
    samples=[]
    for i in range(0,numsamples):
     pos_neg = randint(0,1)
     if pos_neg == 0:
      index = randint(0,len(data[1])-1)
      samples.append((data[1][index],0.0))
     else:
      index = randint(0,len(data[0])-1)
      samples.append((data[0][index],1.0))
    return samples

def get_all_classif_samples(data):
    positives = data[0]
    negatives= data[1]
    samples=[]
    for i in range(0,len(positives)):
        samples.append((positives[i],1.0))
    for i in range(0,len(negatives)):
    
        samples.append((negatives[i],0.0))
    return samples
    

def classif_samples_to_minibatch(samples):
  X = np.zeros((len(samples),len(word_vector.project_text_mod(samples[0][0]))))
  Y = np.zeros((len(samples),1))
  i = 0
  for x in samples:
    sample = x[0]
    outcome = x[1]
    sample_data = word_vector.project_text_mod(sample)
    for j in range(0,len(sample_data)):
      X[i,j] = sample_data[j]
    Y[i,0] = outcome
    i=i+1
  return(X,Y)

def classif_samples_to_minibatch_word(samples):
  X = np.zeros((len(samples),len(word_vector.project_word(samples[0][0].strip()))))
  Y = np.zeros((len(samples),1))
  i = 0
  for x in samples:
    sample = x[0]
    outcome = x[1]
    sample_data = word_vector.project_word(sample.strip())
    for j in range(0,len(sample_data)):
      X[i,j] = sample_data[j]
    Y[i,0] = outcome
    i=i+1
  return(X,Y)

def load_raw_data(filename):
    f = codecs.open(filename+".source","r",'utf-8')
    p = f.readlines()
    for i in range(0,len(p)-1):
        p[i]  = (p[i].strip()).lower()

    f1 = codecs.open(filename+".target","r",'utf-8')
    p1 = f.readlines()
    for i in range(0,len(p1)-1):
        p1[i]  = (p1[i].strip()).lower()

    p2 = []
    for i in range(0,len(p1)-1):
        p2.append((p[i],p1[i]))
    return p2
  
def load_raw_triples(filename):
  #f = codecs.open(filename,"r",'utf-8')
  print("     ... loading lines ...")
  p = []
  with codecs.open(filename,"r",'utf-8') as f:
   count  = 0
   numlines = 0
   for line in f:
    p.append(line)
    count = count + 1
    if count > 100000:
        numlines= numlines + count
        print ("loaded " + str(numlines))
        count = 0
        
    
  
  print("     ... reprocessing data...")
  for i in range(0,len(p)-1):
        p[i]  = (p[i].strip()).lower() 
  result = []
  for i in range(0,len(p)-1):
    result.append(p[i].split("	"))
  return result

def load_all_data(filename1,filename2):
    trainset = load_raw_data(filename1)
    testset = load_raw_data(filename2)
    return(trainset,testset)
general_dict=[]



def gen_dict(data):
    dicton = {}
    for x in data:
        q = x.split()
        for y in q:
            if y in dicton:
                dicton[y] = dicton[y] + 1
            else:
                dicton[y] = 1
    dataset = []
    for key, value in dicton.iteritems():
        dataset.append((value,key))
    darr = sorted(dataset,reverse=True)
    print ("total wordforms = " + str(len(darr)))
    
    return darr[0:30000]

def word2bin(word,dic):
    vect = np.zeros(len(dic)+1)
    p = False
    for i in range(0,len(dic)):
        if word==dic[i]:
            p= True
            vect[i] = 1.0
    if (not p):
        vect[len(vect)-1]=1.0
    return(vect)

   
gdict = {}    
def word2bin_index(word,dic):
    if word in gdict:
      return gdict[word]
    else:
    
     return len(dic)
    #for i in range(0,len(dic)):
    #    if word==dic[i]:
    #        index = i
    #return index
    

def bin2word_max(vect,dic):
  nth = np.argmax(vect)
  if nth < len(dic):
        return (dic[nth])
  else:
      return ("unk")

def bin2word_sample(vect,dic):
    temperature = 1.0
    # helper function to sample an index from a probability array
    vect = np.log(vect) / temperature
    vect = np.exp(vect) / np.sum(np.exp(vect))
    q = np.argmax(np.random.multinomial(1, vect, 1))
    if q < len(dic):
        return (dic[q])
    else:
        return ("unk")



def load_dict(filename):
    f = codecs.open(filename+".dict","r",'utf-8')
    p = f.readlines()
    for i in range(0,len(p)):
        p[i]  = (p[i].strip()).lower()
        gdict[p[i]] = i
    general_dict = p
    return p

def load_dic_vocab(filename):
    f = codecs.open(filename+".dict","r",'utf-8')
    p = f.readlines()
    for i in range(0,30000):
        p[i]  = ((p[i].strip()).lower()).split()[0]
        gdict[p[i]] = i
    general_dict = p
    return p
    

def save_dict(filename,data):
    f = codecs.open(filename+".dict","w",'utf-8')
    for x in data:
        f.write(x[1]+"\n")
    f.close()


def get_random_samples2serial(data,numsamples):
 samples =[]
 for i in range(0,numsamples):
    ind = randint(0,len(data)-3)
    sample = (data[ind],data[ind+1],data[ind+2])
    samples.append(sample)
 return samples

def get_random_samples2serial_inject(data,rawdata,numsamples):
 samples =[]
 for i in range(0,numsamples):
    ind = randint(0,len(data)-3)
    sample = (data[ind],data[ind+1],data[ind+2])
    samples.append(sample)
 for i in range(0,numsamples):
    ind = randint(0,len(rawdata)-3)
    sample = (rawdata[ind],rawdata[ind+1],rawdata[ind+2])
    samples.append(sample)

    
 return samples
    
#convert sample to training vector for recurrent model    
def sample2vector_inputs(samples,maxlen,X,Y,numsample,dic):
    source = samples[numsample][1]
    source_vector = word_vector.project_text_stack(source)
    maxvect = min(len(source_vector),1000)
    source_vector = source_vector[0:maxvect]
    target_words = ["<start>"]
    q = samples[numsample][2].split()
    for x in q:
     target_words.append(x)
    target_words.append("<end>")
    #X = np.zeros((maxlen,len(source_vector)+len(word_vector.project_word(target_words[0]))))
    maxsize = (min(len(target_words)-1,20))
    for i in range(0,20):
        if i < maxsize:
         #vect = word_vector.project_word(target_words[i])
         #q = np.append(source_vector,vect)
         q = word_vector.project_word(target_words[i])
         for j in range(0,len(q)):
             X[numsample,i,j] = q[j]
         num_word = word2bin_index(target_words[i+1],dic)
         Y[numsample,i,num_word] = 1.0
        else:
          ind = len(dic) - 1
          Y[numsample,i,ind] = 1.0
    return (X,Y)

def get_minibatch_for_serial(samples,dic):
    maxlen = 20
    Y= np.zeros((len(samples),maxlen,len(dic)+1))#dtype=np.bool)
    X = np.zeros((len(samples),maxlen,100))
    for i in range(0,len(samples)):
     X,Y=sample2vector_inputs(samples,maxlen,X,Y,i,dic)
    return (X,Y)


                 
#convert sample to training vector for matching model
def sample2vector_class_from_triple(sample):
  X = np.zeros(0)
  X = np.append(X,word_vector.project_text_mod(sample[0]))
  X = np.append(X,word_vector.project_text_mod(sample[1]))
  X = np.append(X,word_vector.project_text_mod(sample[2]))
  return X

def get_minibatch_from_sample_list(sample_list):
  X = np.zeros((len(sample_list),len(sample2vector_class_from_triple(sample_list[0][0]))))
  Y = np.zeros((len(sample_list),1))
  i = 0
  for x in sample_list:
    sample = x[0]
    outcome = x[1]
    sample_data = sample2vector_class_from_triple(sample)
    for j in range(0,len(sample_data)):
      X[i,j] = sample_data[j]
    Y[i,0] = outcome
  #  if outcome == 1:
   #  Y[i,0] = 1.0
   # else:
   #  Y[i,1] = 1.0   
    i=i+1
  return(X,Y)

def get_minibatch_stack(sample_list):
  X = np.zeros((len(sample_list),2000))
  Y = np.zeros((len(sample_list),1))
  i = 0
  for x in sample_list:
    sample = x[0]
    outcome = x[1]
    vect1 = word_vector.project_text_stack(sample[1])
    vect2 = word_vector.project_text_stack(sample[2])
    #vect3 = word_vector.project_text_stack(sample[0])
    sample_data = sample2vector_class_from_triple(sample)
    for j in range(0,min(1000,len(vect1))):
      X[i,j] = vect1[j]
    for k in range(0,min(1000,len(vect2))):
      X[i,k+1000] = vect2[k]
    q = randint(0,8)
    
    #for z in range(0,min(1000,len(vect3))):
      #  X[i,z+2000] = vect3[z]    
      
    Y[i,0] = outcome
  #  if outcome == 1:
   #  Y[i,0] = 1.0
   # else:
   #  Y[i,1] = 1.0   
    i=i+1
  return(X,Y)

def get_minibatch_stack_separate(sample_list):
  X1 = np.zeros((len(sample_list),1000))
  X2 = np.zeros((len(sample_list),1000))
  Y = np.zeros((len(sample_list),1))
  i = 0
  for x in sample_list:
    sample = x[0]
    outcome = x[1]
    vect1 = word_vector.project_text_stack(sample[1])
    vect2 = word_vector.project_text_stack(sample[2])
    #vect3 = word_vector.project_text_stack(sample[0])
    sample_data = sample2vector_class_from_triple(sample)
    for j in range(0,min(1000,len(vect1))):
      X1[i,j] = vect1[j]
    for k in range(0,min(1000,len(vect2))):
      X2[i,k] = vect2[k]
    q = randint(0,8)
    
    #for z in range(0,min(1000,len(vect3))):
      #  X[i,z+2000] = vect3[z]    
      
    Y[i,0] = outcome
  #  if outcome == 1:
   #  Y[i,0] = 1.0
   # else:
   #  Y[i,1] = 1.0   
    i=i+1
    X= {}
    X["source"] = X1
    X["target"] = X2
  return(X,Y)

def get_minibatch_stack_separate2(sample_list):
  X1 = np.zeros((len(sample_list),1000))
  X2 = np.zeros((len(sample_list),1000))
  Y = np.zeros((len(sample_list),1))
  i = 0
  for x in sample_list:
    sample = x
   
    vect1 = word_vector.project_text_stack(sample[1])
    vect2 = word_vector.project_text_stack(sample[2])
    #vect3 = word_vector.project_text_stack(sample[0])
    sample_data = sample2vector_class_from_triple(sample)
    for j in range(0,min(1000,len(vect1))):
      X1[i,j] = vect1[j]
    for k in range(0,min(1000,len(vect2))):
      X2[i,k] = vect2[k]
    q = randint(0,8)
    
    #for z in range(0,min(1000,len(vect3))):
      #  X[i,z+2000] = vect3[z]    
      
    #Y[i,0] = outcome
  #  if outcome == 1:
   #  Y[i,0] = 1.0
   # else:
   #  Y[i,1] = 1.0   
    i=i+1
    X= {}
    X["source"] = X1
    X["target"] = X2
  return(X,Y)

def gen_check_sample(data):
     samples=[]
     validation=[]

     index = randint(0,len(data)-4) 
     
     samples.append(([data[index],data[index+1],data[index+2]],1.0))
     good = (([data[index],data[index+1],data[index+2]],1.0))
     i = 0
     while i<4:
          #index = randint(0,len(data)-1)
          index2 = randint(0,len(data)-4)
          corrupt_sample = ([data[index],data[index+1],data[index2]])#data[index2][2])
          samples.append((corrupt_sample,0.0))
          i = i + 1
     validation.append((corrupt_sample,0.0))
     corrupt = (corrupt_sample,0.0)
     data[index+1] = ""
     data[index+2] = ""
     
     

     return (samples,good,corrupt)

def gen_check_set(num,data):
    sset=[]
    validation = []
    for i in range(0,num):
        ssp,good,corrupt = gen_check_sample(data)
        validation.append(good)
        validation.append(corrupt)
        sset.append(ssp)
    return (sset,validation)
         
             
def gen_sample_list_random(num_samples,data):
    samples=[]
    i = 0
    while i<num_samples :
        outcome = randint(0,1)
       # print (outcome)
        if outcome == 1:
            index = randint(0,len(data)-4)
            samples.append(((data[index],data[index+1],data[index+2]),1.0))
            i = i + 1
        else:
            index = randint(0,len(data)-4)
            index2 = randint(0,len(data)-4)
            corrupt_sample = (data[index],data[index+1],data[index2])#data[index2][2])
            samples.append((corrupt_sample,0.0))
            i=i+1
    return samples
            
        
    
        
    
