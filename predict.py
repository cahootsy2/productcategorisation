import word_vector
import numpy as np
from simple_loader import *
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import Adagrad
from remap import *



if len(sys.argv)<2:
    print "Usage: predict.py <input_file_name> <output_file_name> "
    sys.exit()
else:
    inputfile = sys.argv[1]
    outputfile = sys.argv[2]


print("loading word vectors...")
word_vector.load_projections(getScriptPath()  + "/vectors.txt",getScriptPath()  + "/words.txt")
print("loading categories data")
load_category_list()
#read_docs()
def prediction2cat(vect):
    nth = np.argmax(vect)
    catid = cat_list[nth]
    catname = categories[catid]
    return(catid,catname)

def prediction2cats(vect):
    nths = vect.argsort()[-3:][::-1]
    lst = []
    for x in nths:
        catid = cat_list[x]
        catname = categories[catid]
        lst.append((catid,catname,vect[x]))
    return lst

def predict(model, sample):
    sample = sample2vector_unfolded_p([sample])
    res = model.predict(sample[0],1)
    return res[0]    

print("creating model...")
model = Sequential()
model.add(Dense(500,input_dim=1000,init='uniform'))
model.add(Activation('sigmoid'))
model.add(Dense(len(categories),init='uniform'))
model.add(Activation('sigmoid'))
ada = Adagrad(lr=0.01, epsilon=1e-06)
model.compile(loss='mean_squared_error', optimizer=ada)
print "loading weights"
model.load_weights(getScriptPath() + '/class_model_500.h5')



load_all_merchant_data(inputfile)

def getKey(item):
   return 0-item[1]

def process_category(catid):
    products = all_data[catid]
    mnp = max(100,len(products)-1)
    products = products[0:mnp]
    preds = []
    for product in products:
     # print product
      vect = predict(model, (product.lower(),'47'))
      cats = prediction2cats(vect)
      preds.append(cats)
    all_cats= {}
    catp = []
    
    for x in preds:
        for y in x:
         all_cats[y[0]] = 1
        
    for c in all_cats:
        p = 0.0
        i = 0
        for pr_x in preds:
            for pr in pr_x:
             ct = pr[0]
         #    print ct
        #     print c
             if str(ct)==c:
                 p = p + pr[2]
                 i = i + 1
        p = p / i
        catp.append(((c,categories[c]),p))
    return sorted(catp, key=getKey)        
      
def scan():
    result = "Merchant\tMerchant Category\tMerchant Category ID\tMapping status\t Example Products\t Suggested Cahootsy Category\tAll-Suggested-Categories-that-meet-minimum-probability"
    f = open(outputfile,"w")
    f.write(result+"\n")
    for x in all_data:
        preds = process_category(x)
        print x + " => " + preds[0][0][1]
        p = min(len(preds),3)
        prs = preds[0:p]
        merchant_name = x.split("|")[1]
        merchant_category = x.split("|")[0]
        mcid = x.split("|")[2]
        mapping_status = "Unknown"
        example_products_list = all_data[x]
        example_products = ""
      # mnp = max(100,len(example_products_list)-1)
        for y in example_products_list[0:10]:
            example_products = example_products + "[" + y + "] "
        suggested_categories = ""
        all_suggested_categories = ""
        for p in prs:
            cid = p[0][0]
            lt = p[0][1]
            prob = p[1]
            all_suggested_categories = all_suggested_categories + "["+cid+"]"
            suggested_categories =  suggested_categories + "[" + lt + "=" + cid + "(" + str(prob*100.0) + ") "
        strs = merchant_name  + "\t" +  merchant_category + "\t" + mcid + "\t" + mapping_status + "\t" + example_products + "\t" + suggested_categories + "\t" +  all_suggested_categories
        f.write(strs+"\n")
    f.close()
    
scan()
