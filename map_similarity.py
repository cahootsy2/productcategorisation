import word_vector
import numpy as np
import csv
import glob

print("loading word vectors...")
word_vector.load_projections("vectors.txt","words.txt")

def prepare_text(text):
    p = (text.strip().lower())
    p = p.replace("-"," ")
    p = p.replace(">"," ")
    p = p.replace(","," ")
    p = p.replace("'"," ")
    p = p.replace(" s "," ")
    
    p = p.replace("-"," ")
    return " ".join(list(set(p.split(" "))))

def load_cahootsy_categories():
    f = open("cahootsy_cats.csv","r")
    lines = f.readlines()
    cat_list = []
    for line in lines:
        row = line.split(",")
        full_name = row[3]
        cat_id = row[4]
        cat_list.append((full_name,cat_id))
    return cat_list

def getItem(x):
    return x[0]

def map_category(cat_name,cats):
    scores = [(distance_text(x[0],cat_name),x) for x in cats]
    sorted_scores = sorted(scores,key=getItem,reverse=True)
    return sorted_scores
        
        

def distance_text(text1,text2):
    
    v1 = word_vector.normalize_vector(word_vector.project_text_mod(prepare_text(text1)))
    v2 = word_vector.normalize_vector(word_vector.project_text_mod(prepare_text(text2)))
    q = np.dot(v1,v2)
    return q

def top_similar(word,max_scan):
    dat = []
    i = 0
    for i in range(0,max_scan):
     wordx= word_vector.word_indexes[i]
     d = distance(word,wordx)
     dat.append((d,i))
     
    q = sorted(dat,reverse=True)
    words = []
    for i in range(0,9):
        words.append((q[i][0],word_vector.word_indexes[q[i][1]]))
       # synsets = wordnet.synsets( word_vector.word_indexes[q[i][1]])
       # if len(synsets)>0:
        #    z = synsets[0].definition()
       # else:
        #    z = "unknown"
        print str(q[i][0]) + " " + word_vector.word_indexes[q[i][1]] 
        
                                   
    return words

def load_all_merchant_data(foldername):
    categories={}
    products = {}
    listfiles = glob.glob(foldername)
    print listfiles
    for filename in listfiles:
     with open(filename, 'rU') as f:
      i =0
      qx  = 0
     
      reader = csv.reader(f,delimiter=',')
      for row in reader:
         i = i + 1
         if i>1 and len(row)>4:
          qx = qx + 1
          if qx > 50000:
              print i
              qx = 0
         
          #print row[4]
          if len(row)>27:
           merchant_cat = row[6]
           if len(merchant_cat)>1:
            merchant_category_id = row[5]
            product_name = row[9]
            
            description = row[8]
            merchant_name = row[20]
            if len(merchant_cat)>2:
             id_string =  merchant_category_id + " \t " + merchant_name
             categories[id_string] = (merchant_cat)
             if id_string in products:
                 products[id_string].append(product_name)
             else:
                 products[id_string] = [product_name] 
            
           
    return (categories,products)

def process_and_save(mfilename):
    merchant_cats,mproducts = load_all_merchant_data(mfilename)
    cats = load_good() #load_cahootsy_categories()
    f1 = open("result.csv","w")
    strw = "Merchant category id \t Merchant name \t Merchant category name "
    for i in range(0,10):
        strw = strw + "\t" + "Suggested Cahootsy category " + str(i) + " "

    for i in range(0,10):
        strw = strw + "\t" + "Cahootsy category id " + str(i) + " "

    strw  = strw + "\t Example products"
    
        
    f1.write(strw + " \n" )
    for cat in merchant_cats:
        cahootsy_cats = map_category(merchant_cats[cat],cats)
        string = cat + "\t" + merchant_cats[cat]
        for i in range(0,10):
         string = string + "\t" + cahootsy_cats[i][1][2] + "["+ cahootsy_cats[i][1][1]  + "]" 
        for i in range(0,10):
         string = string + "\t" + "["+ cahootsy_cats[i][1][1]  + "]"
        products = mproducts[cat]
        mlen = min(len(products),6)
        products = " | ".join(products[0:mlen])
        string = string + "\t" + products
        f1.write(string + "\n")
    f1.close()
        
def load_category_tree():
    f = open("cahootsy_tree.csv")
    lines = f.readlines()
    category_tree = {}
    for line in lines:
        row = line.split(",")
        category_id= row[0]
        category_name = row[1]
        slug = row[2]
        category_ancestor = row[6]
        category_tree[category_id] = (category_name,category_ancestor,slug)
    return category_tree

def restore_sense_from_tree(category_tree):
    categories = []
    for x in category_tree:
        parent_id = category_tree[x][1]
        name = category_tree[x][0]
        slug = category_tree[x][2]
        
        if parent_id in category_tree:
            name = category_tree[parent_id][0] + " -> " + name 
            grand_parent_id = category_tree[parent_id][1]
         #   print grand_parent_id
            if  grand_parent_id in category_tree:
                name =  category_tree[rand_parent_id][0] + " -> " + name
                #print name
        categories.append((name,x,slug))
    return categories
        
def load_good():
     tree =  load_category_tree()
     sane_result = restore_sense_from_tree(tree)
     return sane_result
                           
if __name__ == "__main__":
  process_and_save("./data/*.csv")                       
